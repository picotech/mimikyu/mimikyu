# From Imports
from nanite import module_installed, fullpath

# TODO: Switch to using the nanite method of __getattr__ with a list of subcommands
# TODO: Actually, create a submodule metaclass just for the above method, if possible.
seed = getattr(module_installed(fullpath("seed_.py", f_back = 2)), "seed_")
sync = getattr(module_installed(fullpath("sync_.py", f_back = 2)), "sync_")
sync = getattr(module_installed(fullpath("override_.py", f_back = 2)), "override_")