# Imports
import inspect

# From Imports
from autoslot import Slots
from baker.y import git as git_command, git as git_run
from nanite import fullpath
from os import path as os_path, getcwd, environ
from pathlib import Path
from requests import post
from typing import Tuple

class Error(Exception): pass
class telegram_incomplete(Error): pass

class sync_(Slots):
	def __init__(
		self,
		source: bool = getcwd(),
		remote: str = "",
		dry_run: bool = False,
		content: bool = False,
		sub_content: bool = False,
		add: bool = False,
		sub_add: bool = False,
		sub_annex: bool = False,
		telegram: Tuple[str, int] = (
			environ.get("TELEGRAM_TOKEN", None),
			environ.get("TELEGRAM_CHAT_ID", None),
		),
		no_telegram: bool = True,
		main: bool = False,
	):
		self.__source: str = fullpath(source)
		self.__remote = remote
		self.__dry_run: bool = dry_run
		self.__content: bool = content
		self.__sub_content: bool = sub_content
		self.__add: bool = add
		self.__sub_add: bool = sub_add
		self.__sub_annex: bool = sub_annex

		self.__telegram_token: str = telegram[0]
		self.__telegram_chat_id: int = telegram[1]
		if no_telegram:
			self.__telegram = False
		else:
			if None in (self.__telegram_token, self.__telegram_chat_id):
				raise telegram_incomplete('Sorry! Your telegram settings seem to be incomplete! Please check the environment variable "self.__telegram_token" and "self.__telegram_chat_id" and try again!')
			self.__telegram = True

		self.__main = main

	def __call__(self):
		_capture: str = "run" if self.__main else "both"
		git_run.bake_all_(
			_print = True if self.__dry_run else False,
			_capture = _capture if not self.__dry_run else "both",
		)

		git_run.bake_(C = self.__source)
		git_command.bake_(
			C = self.__source,
			_print = False,
			_capture = "both"
		)

		if self.__sub_annex:
			git_run.submodule("sync")
			# Not going to work; looping goes through "stdout", not "stderr", which is what the git annex command would print out to.
			# for submodule in git_command(C = self.__source).submodule(
			# 	"foreach",
			# 	quiet = True,
			# 	_end_args = (
			# 		"'echo $displaypath",
			# 		":"
			# 		"`git",
			# 		"annex",
			# 		"status`'",
			# 	),
			# 	_ignore_stderr = True,
			# ):
			# 	path = fullpath(self.__source, submodule.split(":")[0])
			# 	status = submodule.split(":")[1]
			# 	name = Path(path).stem.partition(".")[0]
			# 	if status.partition("First run")[1] == "First run":
			# 		git_run(C = path).annex("init", name)

			if self.__sub_add:
				git_run.submodule("foreach", "git", "add", ".")
			git_run.submodule(
				"foreach",
				"git",
				"annex",
				"sync",
				content = self.__sub_content
			)

		add_output = str(git_run.add(".")) if self.__add else ""
		prelim_add_output = "\n" if not add_output else ("\n" + add_output + "\n")
		for line in git_command.annex("info"):
			if "[here]" in line:
				device_name = line.split()[-2]
				break

		if not self.__remote:

			remotes_synced = []
			remotes_synced_commands = []

			for remote in git_command.remote():
				if git_command.ls_remote(
					exit_code = True,
					_end_args = remote,
					_verbosity = 1,
					_ignore_stderr = True,
				).return_code == 0:
					print()
					print("=" * len(remote))
					print(remote)
					print("=" * len(remote))
					print()
					remotes_synced_commands.append(
						git_run.annex("sync", content = self.__content, _end_args = remote)
					)
					remotes_synced.append(True)

					if self.__telegram:
						remotes_synced_commands_string = "\n".join(
							str(rsc) for rsc in remotes_synced_commands
						)
				else:
					remotes_synced.append(False)
			if any(remotes_synced) and self.__telegram:
				payload = {
					"chat_id" : self.__telegram_chat_id,
					"text" : f'Ran the following on "{device_name}"' + ":\n" +
							prelim_add_output +
							remotes_synced_commands_string,
					"parse_mode" : "HTML",
				}
				return post(
					f"https://api.telegram.org/bot{self.__telegram_token}/sendMessage",
					data=payload
				).content
			else:
				command_output = str(git_run.annex("sync", content = self.__content))
				if self.__telegram:
					payload = {
						"chat_id" : self.__telegram_chat_id,
						"text" : "No remotes were online, so ran the following on " +
								f'"{device_name}"' +
								":\n" +
								prelim_add_output +
								command_output,
						"parse_mode" : "HTML",
					}
					return post(
						f"https://api.telegram.org/bot{self.__telegram_token}/sendMessage",
						data=payload
					).content
		else:
			if git_command.ls_remote(
				exit_code = True,
				_end_args = self.__remote,
				_verbosity = 1,
				_capture = "both",
				_ignore_stderr = True,
			).return_code == 0:
				command_output = str(git_run.annex(
					"sync",
					content = self.__content,
					_end_args = self.__remote,
				))
				if self.__telegram:
					payload = {
						"chat_id" : self.__telegram_chat_id,
						"text" : f'Ran the following on "{device_name}"' + ":\n" +
								prelim_add_output +
								command_output,
						"parse_mode" : "HTML",
					}
					return post(
						f"https://api.telegram.org/bot{self.__telegram_token}/sendMessage",
						data=payload
					).content
