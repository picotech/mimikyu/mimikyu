# Imports
import click

# From Imports
from nanite import fullpath, module_installed, Command, Option
from os import getcwd, environ
from pathlib import Path

@click.group("kyu", invoke_without_command=True, no_args_is_help=True)
def main():
	pass

@main.command(
	name="seed",
	context_settings=dict(
		ignore_unknown_options=True, allow_extra_args=True
	),
	cls=Command,
	no_args_is_help=True,
)
@click.option("--run", default=True, is_flag=True)
@click.option("-s", "--source", default=getcwd(), type=click.Path())
@click.option("-r", "--reponame")
@click.option("-c", "--precloned", default=False, is_flag=True)
@click.option("-p", "--preseeded", default=False, is_flag=True)
@click.option("-n", "--dry-run", default=False, is_flag=True)
def seed(run, source, reponame, precloned, preseeded, dry_run):

	kyu_class = getattr(
		module_installed(fullpath("seed_.py", f_back = 2)), "seed_"
	)(
		source,
		reponame,
		precloned = precloned,
		preseeded = preseeded,
		dry_run = dry_run,
		main = True,
	)

	kyu_class()

@main.command(
	name="sync",
	context_settings=dict(
		ignore_unknown_options=True, allow_extra_args=True
	),
	cls=Command,
	no_args_is_help=True,
)
@click.option("--run", default=True, is_flag=True)
@click.option("-s", "--source", default=getcwd(), type=click.Path())
@click.option("-r", "--remote", default="")
@click.option("-n", "--dry-run", default=False, is_flag=True)
@click.option("-c", "--content", default=False, is_flag=True)
@click.option("-C", "--sub-content", default=False, is_flag=True, cls=Option, req_all_of=["sub_annex"])
@click.option("-a", "--add", default=False, is_flag=True)
@click.option("-A", "--sub-add", default=False, is_flag=True, cls=Option, req_all_of=["sub_annex"])
@click.option("-S", "--sub-annex", default=False, is_flag=True)
@click.option("-t", "--telegram", nargs=2)
@click.option("-T", "--no-telegram", default=True, is_flag=True)
def sync(run, source, remote, dry_run, content, add, sub_add, sub_annex, sub_content, telegram, no_telegram):

	kyu_class = getattr(
		module_installed(fullpath("sync_.py", f_back = 2)), "sync_"
	)(
		source = source,
		remote = remote,
		dry_run = dry_run,
		content = content,
		sub_content = sub_content,
		add = add,
		sub_add = sub_add,
		sub_annex = sub_annex,
		telegram = telegram if telegram else (
			environ.get("TELEGRAM_TOKEN", None),
			environ.get("TELEGRAM_CHAT_ID", None),
		),
		no_telegram = no_telegram,
		main = True,
	)

	kyu_class()

@main.command(
	name="override",
	context_settings=dict(
		ignore_unknown_options=True, allow_extra_args=True
	),
	cls=Command,
	no_args_is_help=True,
)
@click.option("--run", default=True, is_flag=True)
@click.argument("remote")
@click.option("-s", "--source", default=getcwd(), type=click.Path())
@click.option("-n", "--dry-run", default=False, is_flag=True)
def override(run, remote, source, dry_run):

	kyu_class = getattr(
		module_installed(fullpath("override_.py", f_back = 2)), "override_"
	)(
		remote = remote,
		source = source,
		dry_run = dry_run,
		main = True,
	)

	kyu_class()

if __name__ == "__main__":
	main()
