# From Imports
from addict import Dict as D
from autoslot import Slots
from baker.y import git as git_run, git as git_command
from collections import namedtuple
from gensing import tea
from loguru import logger
from nanite import fullpath
from os import name as os_name, listdir
from pathlib import Path
from toml import load
from typing import Dict, Any

if os_name == "nt":
	from baker.y import robocopy
else:
	from baker.y import rsync


class Error(Exception):
	pass


class submodule_uncommitted(Error):
	pass


class repo_directory_not_empty(Error):
	pass


class seed_(Slots):
	def __init__(
		self,
		source: str,
		reponame: str,
		precloned: bool = False,
		preseeded: bool = False,
		dry_run: bool = False,
		main: bool = False,
	):
		self.__source: str = fullpath(source)
		self.__reponame: str = reponame
		self.__file: Dict[str, Any] = D(load(fullpath(source, ".kyu", reponame + ".kyu")))
		self.__repo: str = fullpath(self.__file.repo.path)
		self.__sourcename: str = self.__file.source.name
		self.__precloned: bool = precloned
		self.__preseeded: bool = preseeded
		self.__dry_run: bool = dry_run
		self.__main = main

	def __call__(self):

		if not listdir(self.__repo):
			raise repo_directory_not_empty("Sorry! The repo directory must be empty!")

		_capture: str = "run" if self.__main else "both"
		git_run.bake_all_(
			_print = True if self.__dry_run else False,
			_capture = _capture if not self.__dry_run else "both",
		)

		git_run.bake_(C = self.__source)
		git_command.bake_(
			C = self.__source,
			_print = False,
			_capture = "both"
		)

		if not self.__precloned:
			if self.__file.submodules.clone:
				for submodule in git_command.submodule(
					"foreach",
					quiet = True,
					_end_args = (
						"'echo $displaypath",
						":"
						"`git",
						"status",
						"--porcelain`'",
					),
				):
					path, status = submodule.split(":")
					if not self.__dry_run:
						if status:
							raise submodule_uncommitted(f'Sorry! Submodule "{path}" seems to have uncommitted changes! Please make sure there are no changes in the submodule and try again!')

			git_run.clone(
				recurse_submodules = self.__file.submodules.clone,
				_end_args = (
					self.__source,
					self.__repo,
				),
			)

		git_run(C = self.__repo).annex("init", self.__reponame)

		if self.__file.submodules.annex:
			submodule_data = namedtuple("submodule_data", "path name")

			submodule_list = []
			for submodule in git_command(C = self.__source).submodule(
				"foreach",
				quiet = True,
				_end_args = ("'echo", "$displaypath'"),
			):
				"""
					Answer: https://stackoverflow.com/questions/31890341/clean-way-to-get-the-true-stem-of-a-path-object/31890400#31890400
					User: https://stackoverflow.com/users/464744/blender
				"""
				submodule_list.append(submodule_data(
					path = fullpath(self.__repo, submodule),
					name = Path(fullpath(self.__repo, submodule)).stem.partition(".")[0]
				))

			for submodule in submodule_list:
				git_run(C = submodule.path).annex("init", submodule.name)

		if not self.__preseeded:
			if os_name == "nt":
				robocopy(
					self.__source,
					self.__repo,
					E = True,
					XD = fullpath(self.__source, ".git"),
				)
			else:
				rsync(
					"-avvczz" if self.__main else "-aczz",
					"-e ssh" if self.__file.repo.remote else "",
					no_links = True,
					exclude = "/.git",
					_end_args = (
						self.__source + "/",
						self.__repo + "/",
					),
				)

		git_run(C = self.__repo).remote("rename", "origin", self.__sourcename)
		git_run(C = self.__source).remote("add", self.__reponame, self.__repo)
		git_run(C = self.__source).annex("copy", fast = True, to = self.__reponame)
