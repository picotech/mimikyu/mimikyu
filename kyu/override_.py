from autoslot import Slots
from baker.y import git as git_run, git as git_command
from nanite import fullpath
from os import getcwd

class override_(Slots):
	def __init__(
		self,
		remote: str,
		source: bool = getcwd(),
		dry_run: bool = False,
		main: bool = False
	):
		self.__remote: str = remote
		self.__source: str = fullpath(source)
		self.__dry_run: bool = dry_run
		self.__main: bool = main

	def __call__(self):
		_capture: str = "run" if self.__main else "both"
		git_run.bake_all_(
			_print = True if self.__dry_run else False,
			_capture = _capture if not self.__dry_run else "both",
		)

		git_run.bake_(C = self.__source)
		git_command.bake_(
			C = self.__source,
			_print = False,
			_capture = "both"
		)

		remote_branches = []
		for rb in git_command.branch(remote = True, list = f"{self.__remote}*"):
			r, b = rb.split("/", 1)
			if r.lstrip() == self.__remote:
				remote_branches.append(f'"{b}"')

		for branch in remote_branches:
			git_run.push(force = True, _end_args = (self.__remote, branch))